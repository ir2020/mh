import firebase from 'firebase'
import firestore from 'firebase/firestore'

var config = {
    apiKey: "AIzaSyC1wfB3L-wZcTCwni1vvXsdYLtPv_SCwiQ",
    authDomain: "authentification-891a9.firebaseapp.com",
    databaseURL: "https://authentification-891a9.firebaseio.com",
    projectId: "authentification-891a9",
    storageBucket: "authentification-891a9.appspot.com",
    messagingSenderId: "492293326268",
    appId: "1:492293326268:web:e1c48d21d4dbb7df9997b9",
    measurementId: "G-6FVMFL6V3J"
  };
  // Initialize Firebase
    const firebaseApp = firebase.initializeApp(config);


  export default firebaseApp.firestore()